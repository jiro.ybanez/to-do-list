<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>To-Do</title>
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet" />
    <!-- MDB -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.4.1/mdb.min.css" rel="stylesheet" />

    <style>
        .completed {
            text-decoration: line-through;
        }
    </style>
</head>
<body>
<section class="vh-100" style="background-color: #3da2c3;">
    <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
            <div class="col col-lg-8 col-xl-6">
                <div class="card rounded-3">
                    <div class="card-body p-4">
                        <p class="mb-2"><span class="h2 me-2">TO-DO LIST</span> <span class="badge bg-danger">checklist</span></p>
                        <ul class="list-group rounded-0" id="todo-list">
                        <div class="input-group mt-3">
                            <input type="text" class="form-control" placeholder="Add a new task" id="new-task" />
                            <button class="btn btn-primary" id="add-task-btn">Add</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- MDB -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.4.1/mdb.min.js"></script>
<script>
    const todoList = document.getElementById("todo-list");
    const addTaskButton = document.getElementById("add-task-btn");
    const newTaskInput = document.getElementById("new-task");

    addTaskButton.addEventListener("click", () => {
        const taskText = newTaskInput.value.trim();
        if (taskText !== "") {
            const listItem = document.createElement("li");
            listItem.className = "list-group-item border-0 d-flex align-items-center ps-0";
            listItem.innerHTML = `
                <input class="form-check-input me-3" type="checkbox" value="" aria-label="..." />
                <span>${taskText}</span>
                <button class="btn btn-danger btn-sm ms-auto delete-btn">Delete</button>
            `;
            todoList.appendChild(listItem);
            newTaskInput.value = "";
        }
    });

    todoList.addEventListener("click", (event) => {
        if (event.target.type === "checkbox") {
            const listItem = event.target.closest(".list-group-item");
            const taskText = listItem.querySelector("span");
            taskText.classList.toggle("completed");
        } else if (event.target.classList.contains("delete-btn")) {
            const listItem = event.target.closest(".list-group-item");
            const confirmDelete = confirm("Are you sure you want to delete this item?");
            if (confirmDelete) {
                listItem.remove();
            }
        }
    });
</script>
</body>
</html>
